const { expect } = require('chai')
const request = require('supertest')
const fs = require('fs')

process.env.STORAGE_FILE = `test/temp/test${Math.random()}.json`
const app = require('../../src/app')

describe('Movies resource', function () {
  this.afterAll('clean up', function () {
    fs.unlinkSync(process.env.STORAGE_FILE)
  })
  describe('GET movies', function () {
    it('should respond with matching movies', async function () {
      const res = await request(app)
        .get('/movies')
        .query({ genres: 'history,War' })

      expect(res.status).to.equal(200)
      expect(res.body).to.have.property('movies')
      expect(res.body.movies[0].id).to.equal(141)
    })

    it('should respond error if invalid genre provided', async function () {
      const res = await request(app)
        .get('/movies')
        .query({ genres: 'programming,War' })

      expect(res.status).to.equal(422)
      expect(res.body)
        .to.have.property('error')
        .which.matches(/validation/i)
    })

    it('should filter by duration', async function () {
      const res = await request(app)
        .get('/movies')
        .query({ duration: 120, genres: 'Action' })

      expect(res.status).to.equal(200)
      expect(res.body).to.have.property('movies').with.lengthOf(4)
      expect(res.body.movies[0].id).to.equal(15)
    })

    it('should respond error if duration is incorrect', async function () {
      const res = await request(app).get('/movies').query({ duration: 'x' })

      expect(res.status).to.equal(422)
      expect(res.body)
        .to.have.property('error')
        .which.matches(/validation/i)
    })

    it('should show only one result when no genres provided', async function () {
      const res = await request(app).get('/movies').query({ duration: 100 })

      expect(res.status).to.equal(200)
      expect(res.body).to.have.property('movies').with.lengthOf(1)
    })
  })

  describe('POST movies', function () {
    // - a list of genres (only predefined ones from db file) (required, array of predefined strings)
    // - title (required, string, max 255 characters)
    // - year (required, number)
    // - runtime (required, number)
    // - director (required, string, max 255 characters)
    // - actors (optional, string)
    // - plot (optional, string)
    // - posterUrl (optional, string)
    it('should respond created movie, when no optional fields provided', async function () {
      const requestBody = {
        genres: ['Thriller'],
        title: 'Test Movie ' + Math.random(),
        year: '1000',
        runtime: 500,
        director: 'Gaga',
      }

      const res = await request(app).post('/movies').send(requestBody)

      // expect(res.status).to.equal(200)
      const want = {
        ...requestBody,
        id: null,
        runtime: '500',
        actors: '',
        plot: '',
        posterUrl: '',
      }
      expect({ ...res.body, id: null }).to.eql(want)
      expect(res.body).to.have.property('id').to.be.a('number')
    })

    it('should respond validation errors movie, when improper data provided ', async function () {
      const requestBody = {
        genres: ['Invalid-Genre'],
        title: 'Test Movie ' + Math.random(),
        year: '',
        runtime: 500,
        director: 'Gaga',
      }

      const res = await request(app).post('/movies').send(requestBody)

      expect(res.body).to.have.property('errors')
      expect(res.body.errors.find((e) => e.param === 'genres')).to.not.equal(
        null,
      )
    })
  })
})
