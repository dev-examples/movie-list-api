const { expect } = require('chai')
const fs = require('fs')
const { FileStorage } = require('../../../src/shared/storage/file-storage')

describe('FileStorage', function () {
  describe('FileStorage', function () {
    const fileName = `test/temp/test${Math.random()}.json`
    this.afterEach('clean up', function () {
      fs.unlinkSync(fileName)
    })

    it('should read from and write to json file', async function () {
      const data = { test: 1 }
      const storage = new FileStorage({ fileName })
      await storage.save(data)
      const have = await storage.read()
      expect(have).to.eql(data)
    })
  })
})
