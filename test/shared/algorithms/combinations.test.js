const { expect } = require('chai')
const {
  getAllCombinations,
} = require('../../../src/shared/algorithms/combinations')

describe('Combinations utility', function () {
  describe('getAllCombinations', function () {
    it('should create a list of all combinations (a powerset without the empty set) from a set, sorted from largest to smallest sets', function () {
      const input = [1, 2, 3]

      const output = getAllCombinations(input)
      const want = [[1, 2, 3], [1, 2], [1, 3], [2, 3], [1], [2], [3]]

      expect(output.length).to.be.equal(Math.pow(2, input.length) - 1)
      expect(output).to.eql(want)
    })
  })
})
