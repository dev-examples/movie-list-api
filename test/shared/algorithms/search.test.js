const { expect } = require('chai')
const {
  filterByNumberWithRange,
  selectRandom,
  findMatchesSorted,
} = require('../../../src/shared/algorithms/search')
const { movies } = require('../../data/movies.json')

describe('Search utility', function () {
  describe('findMatchesSorted', function () {
    it('should filter by given categories, and sort by best matches first', function () {
      const genres = ['History', 'War']

      const output = findMatchesSorted(movies, genres, 'genres')

      expect(output.length).to.equal(11)
      expect(output[0].id).to.eql(141)
    })
  })

  describe('filterByNumberWithRange', function () {
    it('should filter items matching the duration +/- 10', function () {
      const toRuntime = (runtime) => ({ runtime })
      const input = [50, 30, 20, 10, 100, 0].map(toRuntime)

      const output = input.filter(filterByNumberWithRange('runtime', 20, 10))

      const want = [30, 20, 10].map(toRuntime)
      expect(output).to.eql(want)
    })
  })

  describe('selectRandom', function () {
    const movie = selectRandom(movies)
    expect(movies).to.contain(movie)
  })
})
