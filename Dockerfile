FROM node:12-alpine3.11
WORKDIR /app
COPY ./package.json yarn.lock ./
ENV NODE_ENV=production
RUN yarn install
COPY . .
CMD [ "src" ]

