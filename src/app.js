const express = require('express')
// const path = require('path');
const logger = require('morgan')

const { createDataAccessBinding } = require('./middleware/data-access-binding')
const indexRouter = require('./routes/root')
const moviesRouter = require('./routes/movies')

const app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))

app.use('/', indexRouter)

app.use(createDataAccessBinding())
app.use('/movies', moviesRouter)

// error handler
app.use(function (err, req, res, next) {
  // error details only in dev mode
  const details = req.app.get('env') === 'development' ? err : {}

  res.status(err.status || 500)
  res.json({ error: err.message, details })
})

module.exports = app
