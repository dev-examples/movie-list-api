// this router requires use of data-access-binding middleware
// which provides req.daos.movies of type MoviesDAO

const express = require('express')
const { Movie } = require('../../models/movie')
const { validateFindMovies, validateNewMovie } = require('./validations')

const {
  renderValidationErrors,
} = require('../../shared/validation/validation-error-response')

const router = express.Router()

router.get('/', validateFindMovies(), renderValidationErrors(), async function (
  req,
  res,
  next,
) {
  try {
    const { genres, duration } = req.query
    const movies = await req.daos.movies.findMovies({ genres, duration })

    res.json({ movies })
  } catch (err) {
    res.json(400, { error: err.message })
  }
})

router.post('/', validateNewMovie(), renderValidationErrors(), async function (
  req,
  res,
) {
  try {
    const newMovie = new Movie(req.body)
    const movie = await req.daos.movies.createMovie(newMovie)

    res.json({ ...movie })
  } catch (err) {
    res.json(400, { error: err.message })
  }
})

module.exports = router
