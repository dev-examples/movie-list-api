const { check } = require('express-validator')
const {
  validateCategories,
  splitToArray,
} = require('../../shared/validation/validations/categories')

// - a list of genres (only predefined ones from db file) (required, array of predefined strings)
// - title (required, string, max 255 characters)
// - year (required, number)
// - runtime (required, number)
// - director (required, string, max 255 characters)
// - actors (optional, string)
// - plot (optional, string)
// - posterUrl (optional, string)
function validateNewMovie() {
  return [
    check('id').not().exists(),
    check('genres').isArray().exists().bail().custom(validateGenres),
    check('title')
      .isLength({ min: 1, max: 255 })
      .withMessage('Required, max 255 characters.'),
    check('year').isInt().withMessage('Required, should be a number.'),
    check('runtime').isFloat().withMessage('Required, should be a number.'),
    check('director')
      .isLength({ min: 1, max: 255 })
      .withMessage('Required, max 255 characters.'),
    check('actors').isString().optional().withMessage('Should be a string.'),
    check('plot').isString().optional().withMessage('Should be a string.'),
    check('posterUrl').isString().optional().withMessage('Should be a string.'),
  ]
}

function validateFindMovies() {
  return [
    check('genres').custom(validateGenres).customSanitizer(splitToArray),
    check('duration').isNumeric().optional().toFloat(),
  ]
}

async function validateGenres(value, { req }) {
  const allGenres = await req.daos.movies.getGenres()
  return validateCategories(allGenres)(value)
}

module.exports = { validateNewMovie, validateFindMovies }
