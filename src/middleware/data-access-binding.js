const fs = require('fs')
const { FileStorage } = require('../shared/storage/file-storage')
const { MoviesDAO } = require('../data-access-objects/movies-dao')

// returns express middleware that is adding data access objects to req.daos
function createDataAccessBinding() {
  const fileName = process.env.STORAGE_FILE || './data/db-live.json'
  let sourceFileName = './data/db.json'
  if (process.env.NODE_ENV === 'test') {
    sourceFileName = './test/data/movies.json'
  }
  const storage = new FileStorage({ fileName })
  if (!fs.existsSync(fileName)) {
    fs.copyFileSync(sourceFileName, fileName)
  }
  const movies = new MoviesDAO({ storage })
  return function dataAccess(req, res, next) {
    req.daos = { movies }
    next()
  }
}

module.exports = { createDataAccessBinding }
