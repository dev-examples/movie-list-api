const { getAllCombinations } = require('./combinations')

// takes a list and find matches for provided categories,
// sorted by best match (more matching categories first)
function findMatchesSorted(list, selectedCategories, property) {
  const sets = getAllCombinations(convertArrayToUpperCase(selectedCategories))
  const matched = []
  // sets are sorted from larges to smallest, so that we find best matches first
  sets.forEach((set) => {
    list.forEach((item) => {
      const localCategories = convertArrayToUpperCase(item[property])
      if (containsAll(localCategories, set)) {
        // prevent duplicates
        if (!matched.includes(item)) {
          matched.push(item)
        }
      }
    })
  })
  return matched
}

function filterByNumberWithRange(property, start, range) {
  return (item) => {
    const value = item[property]
    if (value >= start - range && value <= start + range) {
      return true
    }
    return false
  }
}

function selectRandom(items) {
  const random = Math.floor(Math.random() * items.length)
  return items[random]
}

function containsAll(list, required) {
  return required.every((r) => list.includes(r))
}

function convertArrayToUpperCase(strings) {
  return strings.map((s) => s.toUpperCase(s))
}

module.exports = {
  filterByNumberWithRange,
  selectRandom,
  findMatchesSorted,
  containsAll,
  convertArrayToUpperCase,
}
