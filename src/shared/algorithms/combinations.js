// takes array of up to 30 elements
// returns a list of all possible combinations (powerset without the empty set)
// takes array as input set,
// returns array of arrays
// O(n*2^n)
function getAllCombinations(baseSet) {
  const w = Math.pow(2, baseSet.length)
  const superSet = []

  for (let mask = 1; mask < w; mask++) {
    const set = []
    baseSet.forEach((item, position) => {
      if (checkMaskBit(mask, position)) {
        set.push(item)
      }
    })
    superSet.push(set)
  }
  return superSet.sort(sortByLength)
}

function checkMaskBit(mask, position) {
  return (mask >> position) % 2
}

function sortByLength(a, b) {
  return b.length - a.length
}

module.exports = { getAllCombinations }
