const fs = require('fs')
const { Semaphore } = require('async-mutex')

function FileStorage({ fileName }) {
  this.fileName = fileName
  this.lock = new Semaphore(100)
}

FileStorage.prototype.save = async function (data) {
  await this.lock.runExclusive(async () => {
    const json = JSON.stringify(data, null, 2)
    fs.writeFileSync(this.fileName, json)
  })
}

FileStorage.prototype.read = async function () {
  const [, release] = await this.lock.acquire()
  try {
    const json = fs.readFileSync(this.fileName, { encoding: 'utf-8' })
    return JSON.parse(json)
  } finally {
    release()
  }
}

module.exports = { FileStorage }
