const {
  containsAll,
  convertArrayToUpperCase,
} = require('../../algorithms/search')

function validateCategories(allCategories) {
  return function (value) {
    // validate query params
    const categories = Array.isArray(value) ? value : splitToArray(value)
    // error if any of the genres not matching

    if (
      !containsAll(
        convertArrayToUpperCase(allCategories),
        convertArrayToUpperCase(categories),
      )
    ) {
      throw new Error(
        'Invalid genres provided. Valid genres are: ' + allCategories.join(','),
      )
    }
    return true
  }
}

function splitToArray(value = '') {
  return value === '' ? [] : value.split(',').map((s) => s.trim())
}

module.exports = { validateCategories, splitToArray }
