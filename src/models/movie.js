function Movie({
  genres,
  title,
  year,
  runtime,
  director,
  actors,
  plot,
  posterUrl,
}) {
  this.genres = genres || []
  this.title = `${title || ''}`
  this.year = `${year || ''}`
  this.runtime = `${runtime || ''}`
  this.director = `${director || ''}`
  this.actors = `${actors || ''}`
  this.plot = `${plot || ''}`
  this.posterUrl = `${posterUrl || ''}`
}

module.exports = { Movie }
