const {
  findMatchesSorted,
  filterByNumberWithRange,
  selectRandom,
} = require('../shared/algorithms/search')

// takes storage of type FileStorage
function MoviesDAO({ storage }) {
  this.storage = storage
}

MoviesDAO.prototype.getGenres = async function () {
  const { genres } = await this.storage.read()
  return genres
}

MoviesDAO.prototype.findMovies = async function ({ duration, genres = [] }) {
  const { movies } = await this.storage.read()

  let found = movies
  if (genres.length > 0) {
    found = findMatchesSorted(movies, genres, 'genres')
  }

  let filtered = found
  if (duration > 0) {
    filtered = found.filter(filterByNumberWithRange('runtime', duration, 10))
  }

  if (filtered.length === 0) {
    return []
  }

  if (genres.length === 0) {
    // if no genres provided, get just one random result
    const random = selectRandom(filtered)
    return [random]
  }
  return filtered
}

MoviesDAO.prototype.createMovie = async function (movieModel) {
  const data = await this.storage.read()
  const id = Math.max(...data.movies.map((m) => m.id)) + 1
  // make sure id is fist but overrides any incoming id
  // eslint-disable-next-line no-dupe-keys
  const movie = { id, ...movieModel, id }
  data.movies.push(movie)
  this.storage.save(data)

  return movie
}

module.exports = { MoviesDAO }
